# Launch rail 
This repository includes the Launch Rail CAD files and the Launch Rail Parts Billing.
## Development
The CAD was developped with Autodesk Fusion 360.
## License
This project is licensed under the [CERN OHLv1.2](https://gitlab.com/white-noise/cronos-rocket/propulsion/-/blob/master/LICENSE)  
